---
title: Only a few weeks left!
date: 2018-09-04
---

All participants should have received an e-mail containing a link to our website! 

**Hello!**

We will use this website to provide you with teaching material, scripts and data. As we are still building it, there is only limited content so far. But I already added some usefull links and tutorial on getting started with R (see "TUTORIAL" above).

