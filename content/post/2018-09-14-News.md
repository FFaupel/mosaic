---
title: Get ready for the 8th of october!
date: 2018-09-14
---

The website now contains new information about how to prepare your data for using them at the Research School *(see Material / Data Collections)*. It also provides a range of useful cheatsheets *(see Learning R / Cheat sheets)*.

New updates coming soon ;)
