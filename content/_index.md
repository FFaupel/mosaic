##    (8th) 9th - 13th october 2018

### Networks in Archaeological Research

Ranging from exchange to urban networks, interactions
systems are a basic component of every society,
from the prehistoric to the modern. The Research
School ‘MOSAICnet: Networks in archaeological
research’ aims at bringing together young and senior
researchers around this transdisciplinary issue of
networks. The Research School oers the state of the
art of several methodological approaches (such as
Social Network Analysis, Spatial Network Analysis, etc.),
resulting from the dialogue between the Human and
the Exact Sciences. It also intends to discuss their
contributions to the research on past social, cultural
and economic interactions.

### Organiser

Oliver Nakoinz, Katherine Gruel,
Vincent Guichard, Clara Filet and Franziska Faupel
