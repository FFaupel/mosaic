---
title: R Scripts
subtitle: Bloc 0 & 3
comments: false
---

The following link will guide you to our R Scripts folder. There you will find a bunch of scripts like for the introduction or exercises. Don't worry, you don't have to understand the content before the Research School! Some of the scripts are rather short and not self explanatory, as we will teach R by doing **live coding**. Meaning, we will show you how to do it, then you have time to redo the task and then to start with an exercise. Whenever you struggle, we will be there to help you! These live coding segments will take 5 to 10 minutes. 


<a href="https://gitlab.com/oliver.nakoinz/MOSAICnet2018/tree/master/1script">R Scripts</a>

