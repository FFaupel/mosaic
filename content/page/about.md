---
title: About MOSAICnet
subtitle: Research School AOROC-Bibracte 4
comments: false
---

## Networks in Archaeological Research

Ranging from exchange to urban networks, interactions
systems are a basic component of every society,
from the prehistoric to the modern. The Research
School ‘MOSAICnet: Networks in archaeological
research’ aims at bringing together young and senior
researchers around this transdisciplinary issue of
networks. The Research School oers the state of the
art of several methodological approaches (such as
Social Network Analysis, Spatial Network Analysis, etc.),
resulting from the dialogue between the Human and
the Exact Sciences. It also intends to discuss their
contributions to the research on past social, cultural
and economic interactions.

The Research School will last 6 days, and will address
the matters of the theoretical context of the research
on interaction systems, the empirical data we can rely
on to grasp them, as well as provide an overview of
several available methods of system analysis and
reconstruction. Those tools will be applied using the
Rstudio software.

## Organisation

### Institutes

- Bibracte European Research Centre
- Laboratoire AOROC ENS Paris
- Université Franco-Allemande / Deutsch-Französische Hochschule
- CNRS
- Institute of Pre- and Protohistoric Archaeology
- Johanna Mestorf Academy - Kiel University

### Organisation Committee

- Katherine Gruel
- Oliver Nakoinz
- Vincent Guichard
- Clara Filet 
- Franziska Faupel

### Scientific Committee

- Oliver Nakoinz, Privat-docent, Institute of Pre- and Protohistoric Archaeology, Kiel University, Germany
- Katherine Gruel, Research Director CNRS Ecole Normale Supérieure, France
- Clara Filet, PhD candidate Paris 1 University, France
- Franziska Faupel, PhD candidate Kiel University, Germany

### Guest Lecturers

- Olivier Buchsenschutz, Emeritus Research Director CNRS, Ecole Normale Supérieure, France
- Patrice Brun, University Professor Paris 1 University, France
- Fabrice Rossi, University Professor Paris 1 University, France

