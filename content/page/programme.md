---
title: Programme of MOSAICnet
subtitle: Research School AOROC-Bibracte 4
comments: false
---

The Research School will last 6 days, and will address
the matters of the theoretical context of the research
on interaction systems, the empirical data we can rely
on to grasp them, as well as provide an overview of
several available methods of system analysis and
reconstruction. Those tools will be applied using the
R Studio software.


![Programme](/programme.png)
