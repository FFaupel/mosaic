---
title: Data Collections
subtitle: Which data will be used for MOSAICnet?
comments: false
---

During the workshops, the working groups will practice several exercises
based on the methods introduced during the lectures. Each group will have
the opportunity to work on their own data, or to use provided datasets.


## I want to use my own data
Good! You will then be able to estimate to what extent each method is
relevant for your own research project

For using your own dataset, you will need to work with this type of dataset
(please provide the following structure)

![dataexample_1](/dataexample_1.png)

this is an example of filled table:

![dataexample_2](/dataexample_2.png)

Such file can be saved in .xls .xlsx or .csv



## I don't want to use my own data
No Problem! Training data will be provided.