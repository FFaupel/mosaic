---
title: Tutorials
subtitle: How to ... ?
comments: false
---

As most of our participants are unfamiliar to R, we collected some helpfull tutorials. Prior starting to explore R, installing R is mandatory. Additionally, R Studio needs to be installed as we will work with R Studio during the course.

### R: The R Project for Statistical Computing

R is a free software environment for statistical computing and graphics. It compiles and runs on a wide variety of UNIX platforms, Windows and MacOS.

Please install R using this link:

[Installing R](https://ftp.fau.de/cran/)

### R Studio

RStudio makes R easier to use. It includes a code editor, debugging & visualization tools.

Please install R studio using this link:

[R Studio](https://www.rstudio.com/products/rstudio/download/)

### Some Tutorials ...

Here are two tutorials, which you can use to get to know R (you don’t have to!):

[R Tutorial](http://www.r-tutor.com/r-introduction)

[You Tube Tutorial](https://www.youtube.com/watch?v=5YmcEYTSN7k)

[Introduction à R Studio](https://www.youtube.com/watch?v=1mSzskE1TWs)

[Introduction au langage R](https://r.developpez.com/tutoriels/introduction-r/)

And **R for Beginners**, where you can also learn how to use R.

[R for Beginners](https://cran.r-project.org/doc/contrib/Paradis-rdebuts_en.pdf)

If you run into any problems installing R, please let us know. We will try to solve it together.


