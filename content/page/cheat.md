---
title: Cheat Sheets
subtitle: How to ... ?
comments: false
---

The following **cheat sheets** are quite usefull when working with R. There are much more available online...

<a href="/data-import.pdf">Importing Data</a>

<a href="/data-transformation.pdf">Data Transformation</a>

<a href="/purrr.pdf">Purrr</a>

<a href="/rstudio-ide.pdf">R Studio</a>

<a href="/strings.pdf">Strings</a>

<a href="/04 Basic - Lists Cheat Sheet.pdf">Handeling Lists</a>

<a href="/data-visualization-2.1.pdf">Data Visualization</a>

<a href="/ggplot2-cheatsheet-2.0.pdf">ggplot</a>



