---
title: Lectures
subtitle: Bloc 0 & 3
comments: false
---

## Bloc 0: Introduction to R
The following links will guide you to our Introduction to R. Don't worry, you don't have to understand the content before the Research School! Some of the presentations are rather short, as we will teach R by doing **live coding**. Meaning, we will show you how to do it, then you have time to redo the task and then to start with an exercise. Whenever you struggle, we will be there to help you! These live coding segments will take 5 to 10 minutes. 

### What is R, how can I use it and why should I?!

That will be covered in the first part of our introduction to R. We will explain you, why we think that R is such an amazing tool to do statistic, no matter if classic statistics, multivariate or spatial analysis.

<ul>
  <li>Why using R?</li>
  <li>History of R</li>
  <li>7 Levels of Learning</li>
  <li>R Studio</li>
  <li>Scripts</li>
</ul>

<a href="https://gitlab.com/oliver.nakoinz/MOSAICnet2018/blob/master/9praes/PDFofLectures/Rintro01.pdf">What is R?!</a>


### Working with R

During this lecture we will introduce the basic objects in R and how to use them. How can functions and objects differentiated? How do I know that a variable exists? 

<ul>
  <li>Function vs Objects</li>
  <li>Vectors</li>
  <li>Functions</li>
  <li>Operators</li>
  <li>Data Frames and Matrices</li>
</ul>


<a href="https://gitlab.com/oliver.nakoinz/MOSAICnet2018/blob/master/9praes/PDFofLectures/Rintro02.pdf">Working with R</a>


### First Calculations and Plots in R

Now, it gets more interesting! We show you how to handle data. You will learn how R efficiently summarises and represent your data and you will do first statistical tests.

<ul>
  <li>Package Management</li>
  <li>Loading Data</li>
  <li>Data Management</li>
  <li>Loops and Restrictions</li>
  <li>Some Statistics</li>
  <li>Digression: Tidy verse</li>
  <li>Plotting in R</li>
</ul>

<a href="https://gitlab.com/oliver.nakoinz/MOSAICnet2018/blob/master/9praes/PDFofLectures/Rintro03.pdf">First Calculations</a>


### R and Spatial Data

As we are working with spatial data during our course, we will give you a short introduction into Spatial Data in R. After this course you will be able to load and map spatial data nicely. Spatial Analysis will be done later in Bloc 3, for example.

<ul>
  <li>Load Spatial Objects</li>
  <li>Create Spatial Objects</li>
  <li>Manipulate Spatial Objects</li>
  <li>Plot Spatial Objects</li>
</ul>

<a href="https://gitlab.com/oliver.nakoinz/MOSAICnet2018/blob/master/9praes/PDFofLectures/Rintro04.pdf">R and Spatial Data</a>

## Bloc 1: Networks in Archaeology - considerations and challenges (O. Nakoinz, P. Brun and O. Buchsenschutz)

Presentations on the theoretical context of the research on interactions and networks in past societies. Several types of networks and the means to identify and reconstruct them, as well as non-computer-based approaches on spatial interactions are considered.

<ul>
  <li>Welcome and introduction (Organisation Committee)</li>
  <li>Theoretical context of archaeological networks (P. Brun)</li>
  <li> Archaeology of exchange systems: which data? (O. Buchsenschutz)</li>
</ul>

<a href="https://gitlab.com/oliver.nakoinz/MOSAICnet2018/blob/master/9praes/PDFofLectures/Network_approach.pdf">Bloc 1, P. Brun</a>

## Bloc 2: Social Network Analysis (O. Nakoinz)

Introduction to Social Network Analysis in theory and practice. Basic concepts, the handling and visualisation of network data, and different analytical approaches are presented. Centrality indices, cliques and cluster as well as network distances are discussed. After the lectures, the discussed methods are explored by applying them to different data in different working groups.

<a href="https://gitlab.com/oliver.nakoinz/MOSAICnet2018/blob/master/9praes/PDFofLectures/Bloc2.pdf">Social Network Analysis</a>

## Bloc 3: Exchange Systems in Space

Past interactions are constrained by the distance between actors. Distances had to be crossed by at least one person to faciliate exchange, lacking modern communication techniques. The physical networks between interacting partners is crucial for this network to operate. Where were the path located? Who was connected directly? Most archaeological remains of infrastructures (like path, roads, buildings associated with traveling) are rare, isolated and seldomly preserved. Hence, reconstructing a path system using empirical data will help to understand and interpret the infrastructure. 

But, how to interpret the path system? Using miscellaneous friction raster to model parameters likely to influence an infrastructure, parameters with low conformation to empirical data can be excluded. The best fitting model confirms (logically) best with empirical data. It is likely that these parameter influenced the parameter of path systems.

### Reconstructing Path Systems 

In the first part of this lecture, a method to reconstruct path systems is presented. 

<a href="https://gitlab.com/oliver.nakoinz/MOSAICnet2018/blob/master/9praes/PDFofLectures/Bloc3Part1.pdf?expanded=true&viewer=rich">Bloc 3, Part 1</a>

### Evaluating Parameter of Path Systems

The construction and modelling of friction raster, applying Random Walk approach and evaluating the miscellaneous models will be covered in the second part.

<a href="https://gitlab.com/oliver.nakoinz/MOSAICnet2018/blob/master/9praes/PDFofLectures/Bloc3Part2.pdf?expanded=true&viewer=rich">Bloc 3, Part 2</a>


## Bloc 4: Approaching the organization of exchange systems (C. Filet, F. Rossi)

Most of past interactions, their actors and their intensity are not known to archaeologists. The course will be dedicated to ways of approaching the organization of past exchange systems in the absence of written sources. For this purpose an introduction to spatial interaction models and the study of distribution patterns of archaeological artefacts will be given.

<a href="https://gitlab.com/oliver.nakoinz/MOSAICnet2018/blob/master/9praes/PDFofLectures/Bloc4Intro.pdf">Bloc 4, Introduction</a>

<a href="https://gitlab.com/oliver.nakoinz/MOSAICnet2018/blob/master/9praes/PDFofLectures/Bloc4Part2.pdf">Bloc 4, Data Modelling</a>

<a href="https://gitlab.com/oliver.nakoinz/MOSAICnet2018/blob/master/9praes/PDFofLectures/Bloc4Part3.pdf">Bloc 4, Theory Modelling</a>

The scripts showing how to do the analysis, can be found in <a href="https://gitlab.com/oliver.nakoinz/MOSAICnet2018/tree/master/1script">1script/</a>